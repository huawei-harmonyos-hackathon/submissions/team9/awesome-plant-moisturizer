# Awesome Plant Watering System

## Description

Awesome Plant Watering System is a IoT project that provides you with a gardening tool to water your plants. This project is modular, so it consists of a base module, which controls the watering of the plant, according to a given condition, and a variety of modules, to help you adjust to more complex environmental conditions.

## Modules

Modules are simple additional devices, that can be added to the base, to expand its functionality.

#### Base Module

This is a basic module, which controls the watering. It connects all the additional modules, and manages the communication between them.

#### Watering Module

This is a basic module, which automates watering. It pumps the water from the water tank to the pot, whenever the base module tells it to.

#### Soil Moisture Control Module

This module checks the moisture of the soil with two electrodes inserted in the ground and sends the data to one of the main module's analog pins.

#### Temperature Control Module

This module checks the temperature of the air and sends the data to one of the main module's analog pins.

#### Light Control Module

This module checks the light conditions, and sends the data to one of the main module's analog pins.

**The data from aforementioned sensors is then used to calculate proper amount and frequency of watering.**

#### Water Level Control

This module checks the water level in the water tank - this data is then used to message the user to refill the tank.

#### Home Assistant Module

This module connects the base to the local network via wifi connection, and enables it to be controlled remotely with Home Assistant. It is compatible with other modules, so you can set the watering parameters according to the other modules' sensors.

## Usage

Just insert the input end of the water pipe to the water tank, and the other to the pot with your awesome plant. Connect the base and pump to the power supply, and you're good to go. If you have the Home Assistant Module, you should connect it to your mobile app (still under construction).

## Roadmap
The project, as it is quite vast, is yet to be complete, but so far we've come up with the following roadmap
- [x] Base Module
- [x] Watering Module
- [x] Humidity Control Module
- [ ] Temperature Control Module
- [ ] Light Control Module
- [ ] Water Level Control
- [ ] Home Assistant Module

## Authors and acknowledgment
Michał Filipiak and Filip Tłuszcz

