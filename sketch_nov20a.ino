// constants won't change
const int RELAY_PIN = A5; 
const int SENSOR_PIN = A0; 

const int output_value;
const int threshold = 50;

int pumping_volume = 100;
int flow = 33;

void setup() {
  Serial.begin(9600);
  Serial.println("Reading From the Sensor ...");
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, HIGH);
}

// the loop function runs over and over again forever
void loop() {
  Serial.println(output_value);
  delay(2000);
  output_value= analogRead(SENSOR_PIN);
  output_value = map(output_value, 550, 0, 0, 100);
  if(output_value < threshold && output_value > 0) {
    Serial.println("watering");
    digitalWrite(RELAY_PIN, LOW);
    delay( 1000 * pumping_volume / flow );
    digitalWrite(RELAY_PIN, HIGH);
    delay(3600000); //wait an hour to avoid overwatering
  }

}
